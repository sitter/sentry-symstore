#!/usr/bin/env ruby
# frozen_string_literal: true

# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

require 'tty/command'
require 'net/sftp'
require 'tmpdir'
require 'json'
require 'sentry-ruby'
require 'faraday'

Sentry.init do |config|
  config.dsn = 'https://79d4e12bee78473caa65b4c83f32fba2@errors-eval.kde.org/784'
  config.traces_sample_rate = 0.1
end
at_exit do
  Sentry.capture_exception($!, level: :fatal) unless $!.is_a?(SystemExit) || $!.nil?
  Sentry.session_flusher.flush
end

# Send a heartbeat
sentry = Faraday.new do |c|
  c.request :json
  c.headers['Authorization'] = "DSN #{Sentry.configuration.dsn}"
end
sentry.post('https://errors-eval.kde.org/api/0/organizations/kde/monitors/symstore/checkins/', status: 'ok')

# Make sure pip3's bin path is in PATH
ENV['PATH'] = "#{Dir.home}/.local/bin:#{ENV.fetch('PATH')}"
STORE_PATH = "#{Dir.pwd}/"
MANIFEST_PATH = "#{STORE_PATH}/manifest.json"

manifest = []
manifest = JSON.parse(File.read(MANIFEST_PATH)) if File.exist?(MANIFEST_PATH)

urls = []
Net::SFTP.start('origin.download.kde.org', 'ftpneon') do |sftp|
  sftp.dir.glob('stable/release-service/', '*/windows/*-dbg.*').each do |name|
    urls << "https://download.kde.org/stable/release-service/#{name.name}"
  end

  sftp.dir.glob('stable/ruqola/', '*windows*-dbg.*').each do |name|
    urls << "https://download.kde.org/stable/ruqola/#{name.name}"
  end
end

cmd = TTY::Command.new
cmd.run('pip3', 'install', 'symstore')

urls.each do |url|
  next if manifest.include?(url)
  next unless url.end_with?('.7z')

  Dir.mktmpdir do |tmpdir|
    cmd.run('wget', '--quiet', url, chdir: tmpdir)
    cmd.run('7z', 'e', File.basename(url), '-y', chdir: tmpdir)
    cmd.run('symstore', STORE_PATH, *Dir.glob("#{tmpdir}/*.pdb"))
  end

  manifest << url
end

File.write(MANIFEST_PATH, JSON.generate(manifest))

cmd.run('rsync', '-a', '--delete', "#{Dir.pwd}/.", 'symstore@upload.kde.org:/srv/www/cdn.kde.org/symstore/')
